local S = core.get_translator("balloon_bop")

local storage = core.get_mod_storage()


function balloon_bop.store_scores(data)
    storage:set_string("scores", core.serialize(data))
end
function balloon_bop.get_scores()
    return core.deserialize(storage:get_string("scores"))
end

-- _bop.scores is a table with the forllowing structure:
-- {
--     [arena_name] ={
--         [p_name]=score,
--         [p_name]=score,
--         ..
--     },
--     [arena_name] ={
--         [p_name]=score,
--         [p_name]=score,
--         ..
--     },
--     ..
-- }

balloon_bop.scores = balloon_bop.get_scores() or {}


local function create_score_table(ordered_names, scoredata, empty_text, select_player)
    local content = ""
    local prev_score = nil
    local rank = 0
    local skipranks = 0
    local sel_idx = nil
    if #ordered_names >=1 then
        for idx, u_name in ipairs(ordered_names) do
            local score = scoredata[u_name]
            if score ~= prev_score then
                rank = rank + 1 + skipranks
                skipranks = 0
            else
                skipranks = skipranks + 1
            end
            prev_score = score

            local color = ""
            -- index for table selection
            if u_name == select_player then
                sel_idx = idx
                -- highlight row if this is the player we're looking for
                color = "#f47e1b"
            end

            -- write row: color, rank, player name, score
            content = content .. color .. "," .. rank ..","..core.formspec_escape(u_name)..","..score

            -- next row
            if idx ~= #ordered_names then
                content = content .. ","
            end
        end
    else
        return ",,"..core.formspec_escape(empty_text), sel_idx
    end
    return content, sel_idx
end

local function get_ordered_names(arena_name, data, ignore_zero)
    -- sort the scores
    local ordered_names = {}
    for p_name, score in pairs(data) do
        if score > 0 or not ignore_zero then
            table.insert(ordered_names, p_name)
        end
    end
    table.sort(ordered_names, function(a,b)
        return data[a] > data[b]
    end)
    return ordered_names
end

local formspec_prefix = "formspec_version[5]"..
    "size[10.5,10.5]"..
    "background9[-.5,-.5;11.5,11.5;balloon_bop_leader_bg.png;false;125]"..
    -- content_offset=0 to prevent button from moving the text when clicked
    "style_type[button;border=false;content_offset=0]"..
    "style_type[button,table;font=normal,bold]"..
    "style[hs_title;font_size=+3]"..
    "style[hs_title;textcolor=#e6482e]"..
    "style[arena_name;textcolor=#f4b41b]"..
    "tableoptions[color=#CFC6B8;highlight_text=#CFC6B8;highlight=#00000000;border=false;background=#00000010]"..
    "tablecolumns[color;text,align=right;text,padding=2;text,align=right,padding=4]"


function balloon_bop.get_leader_form(arena_name, select_player)
    local p_names = "<no data>"
    local sel_idx

    if balloon_bop.scores[arena_name] then
        local data = balloon_bop.scores[arena_name]
        local ordered_names = get_ordered_names(arena_name, data, true)
        p_names, sel_idx = create_score_table(ordered_names, data, S("The leaderboard is empty."), select_player)
    end
    if not sel_idx then
        sel_idx = 1
    end

    return formspec_prefix ..
    -- fake buttons to create centered text (clicking them does nothing)
    "button[0.6,0.6;9.3,0.8;hs_title;"..core.formspec_escape(S("Balloon Bop Leaderboard")).."]"..
    "button[0.6,1.6;9.3,0.8;arena_name;"..arena_name.."]"..

    "table[0.6,2.5;9.3,7.4;names;"..p_names..";"..sel_idx.."]"

end



function balloon_bop.get_leader_form_endgame(arena_name, l_data, select_player)
    local p_sel_idx
    local lp_sel_idx
    local p_names = "<no data>"
    local lp_names = "<no data>"

    -- leaderboard
    if balloon_bop.scores[arena_name] then
        local data = balloon_bop.scores[arena_name]
        local ordered_names = get_ordered_names(arena_name, data, true)
        p_names, p_sel_idx = create_score_table(ordered_names, data, S("The leaderboard is empty."), select_player)
    end

    -- scoreboard for the current round
    if l_data then
        local ordered_names = get_ordered_names(arena_name, l_data, false)
        lp_names, lp_sel_idx = create_score_table(ordered_names, l_data, S("The scoreboard is empty."), select_player)
    end
    if not p_sel_idx then
        p_sel_idx = 1
    end
    if not lp_sel_idx then
        lp_sel_idx = 1
    end

    return formspec_prefix ..
    -- fake buttons to create centered text (clicking them does nothing)
    "button[0.6,0.6;9.3,0.8;hs_title;"..core.formspec_escape(S("Balloon Bop Leaderboard")).."]"..
    "button[0.6,1.6;9.3,0.8;arena_name;"..arena_name.."]"..
    "style[this,high;textcolor=#71aa34]"..
    "button[0.6,2.7;9.3,0.6;this;"..core.formspec_escape(S("This Game")).."]"..
    "table[0.6,3.3;9.3,2;l_names;"..lp_names..";"..lp_sel_idx.."]"..
    "button[0.6,5.5;9.3,0.6;high;"..core.formspec_escape(S("Leaderboard")).."]"..
    "table[0.6,6.1;9.3,3.8;g_names;"..p_names..";"..p_sel_idx.."]"
end



core.register_chatcommand("balloonbopscores", {
    params = "<"..S("arena name")..">",  -- Short parameter description

    description = S("Show Ballon Bop leaderboard"),  -- Full description

    func = function(name, param)
        if param then
            if balloon_bop.scores[param] then
                core.show_formspec(name, "bb_scores", balloon_bop.get_leader_form(param, name))
            else
                return false, S("[!] No data for that arena or that arena does not exist!")
            end
        end
    end,
})
