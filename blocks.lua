local S = core.get_translator("balloon_bop")
-- Detect creative mod --
local creative_mod = core.get_modpath("creative")
-- Cache creative mode setting as fallback if creative mod not present --
local creative_mode_cache = core.settings:get_bool("creative_mode")

-- Returns a on_secondary_use function that places the balloon block in the air -- 

local soundsConfig = function ()
	return {
		footstep = {name = "balloon_bop_footstep", gain = 0.2},
		dig = {name = "balloon_bop_footstep", gain = 0.3},
		dug = {name = "balloon_bop_footstep", gain = 0.3},
		place = {name = "balloon_bop_footstep", gain = 1.0}
	}
end

-- Holds balloonblock functions and config --
local state = {
	groups = {snappy=3, fall_damage_add_percent = -99, bouncy=70}
}
-- Normal balloon_bop --

core.register_node("balloon_bop:red", {
  description = S("Red balloon"),
  tiles = {"balloon_bop_red.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	sounds = state.sounds
})


core.register_node("balloon_bop:yellow", {
	description = S("Yellow balloon"),
	tiles = {"balloon_bop_yellow.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	sounds = state.sounds
})


core.register_node("balloon_bop:green", {
  description = S("Green balloon"),
  tiles = {"balloon_bop_green.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	sounds = state.sounds
})

core.register_node("balloon_bop:blue", {
  description = S("Blue balloon"),
  tiles = {"balloon_bop_blue.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	sounds = state.sounds
})



core.register_node("balloon_bop:black", {
  description = S("Black balloon"),
  tiles = {"balloon_bop_black.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	sounds = state.sounds
})



core.register_node("balloon_bop:white", {
  description = S("White balloon"),
  tiles = {"balloon_bop_white.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	sounds = state.sounds
})



core.register_node("balloon_bop:orange", {
  description = S("Orange balloon"),
  tiles = {"balloon_bop_orange.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	sounds = state.sounds
})


core.register_node("balloon_bop:purple", {
  description = S("Purple balloon"),
  tiles = {"balloon_bop_purple.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	sounds = state.sounds
})


core.register_node("balloon_bop:grey", {
  description = S("Grey balloon"),
  tiles = {"balloon_bop_grey.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	sounds = state.sounds
})


core.register_node("balloon_bop:pink", {
  description = S("Pink balloon"),
  tiles = {"balloon_bop_pink.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	sounds = state.sounds
})



core.register_node("balloon_bop:brown", {
  description = S("Brown balloon"),
  tiles = {"balloon_bop_brown.png"},
	groups = state.groups,
	paramtype = "light",
	sunlight_propagates = true,
	sounds = state.sounds
})
