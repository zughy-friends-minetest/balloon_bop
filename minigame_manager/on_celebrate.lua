local S = core.get_translator("balloon_bop")

arena_lib.on_celebration("balloon_bop", function(arena, winners)
    for pl_name,stats in pairs(arena.players) do -- it is a good convention to use "pl_name" in for loops and "p_name" elsewhere
        if balloon_bop.infohuds[pl_name] then
                local player = core.get_player_by_name(pl_name)
                if player then
                        -- clear HUDs
                        player:hud_remove(balloon_bop.infohuds[pl_name])
                        for idx,hud_id in ipairs(balloon_bop.numhuds[pl_name]) do
                                player:hud_remove(hud_id)
                        end
                        balloon_bop.infohuds[pl_name] = nil
                        balloon_bop.numhuds[pl_name] = nil
                end
        end
        core.chat_send_player(pl_name,S("Game Over! Your score is @1!", arena.players[pl_name].score))
    end
    for _,obj in pairs(core.get_objects_inside_radius(arena.balloon_spawner, arena.arena_radius)) do
            local ent = obj:get_luaentity()
            if ent and ent.name == "balloon_bop:balloon" then
                    obj:remove()
            end
    end
end)
